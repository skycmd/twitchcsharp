**TwitchCSharp is a C# Wrapper for accessing the Twitch v3 REST API**

#Documentation
Official documentation for accessing the Twitch-API can be found at https://github.com/justintv/Twitch-API


#Explanation


##Clients

Clients are the interfaces to performing API requests.

Currently there are three client classes:

###TwitchReadOnlyClient

:    This is a read-only client which can be used to access information available publicly.

     A Client-ID must be specified.

###TwitchAuthenticatedClient

:    This inherits from the read-only client, however requires an OAuth token.

     When authenticated, this client may access data and perform actions based on the permissions of the OAuth token.


###TwitchNamedClient

:    **Deprecated: Use TwitchAuthenticatedClient instead.**

:    This class inherits from the authenticated client, but allows you to specify a username.


#Acknowledgements

## Credits

This repo is a fork of TwitchCSharp by michidk (https://github.com/michidk/TwitchCSharp).

Licensed under the Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0). Copyright 2015 michidk.

## Used DLL's

http://restsharp.org/

http://www.newtonsoft.com/json